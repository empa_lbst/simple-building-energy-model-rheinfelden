close all; clear all; clc

%set the number of buildings/zones
number_of_buildings = 50;
number_of_buildings_per_set = 10;
number_of_zones = 1;

%set the paths of the output from the occupancy model
occupancy_data_stem = 'C:\EnergyPlusV8-2-7\Experiments\Data_OccupancyModelInput\Rheinfelden\Rheinfelden_FiftyBuildings_Part';

%set the path for saving the processed occupancy data
BEM_inputs_path = 'C:\EnergyPlusV8-2-7\Experiments\BEM_Inputs\';

%set the name/location of the template IDF file
template_IDF = 'H:\projects\DemandUncertaintyModel\BuildingDemandModel\BEM_Templates\Rheinfelden_Template_Simplified.idf';

%set the path of the new IDF file to be created
new_IDF_path = 'C:\EnergyPlusV8-2-7\Experiments\BEM_Output\';

%set the name of the new IDF file to be created
new_IDF_name = 'Rheinfelden.idf';

%% DELETE THE OLD RESULTS

for zone=1:number_of_zones
    if exist(strcat(BEM_inputs_path,'ActiveOccFile_zone',num2str(zone),'.csv'), 'file')==2, 
        delete(strcat(BEM_inputs_path,'ActiveOccFile_zone',num2str(zone),'.csv')); 
    end
    if exist(strcat(BEM_inputs_path,'SleepingOccFile_zone',num2str(zone),'.csv'), 'file')==2, 
        delete(strcat(BEM_inputs_path,'SleepingOccFile_zone',num2str(zone),'.csv')); 
    end
    if exist(strcat(BEM_inputs_path,'SetpointsFile_zone',num2str(zone),'.csv'), 'file')==2, 
        delete(strcat(BEM_inputs_path,'SetpointsFile_zone',num2str(zone),'.csv')); 
    end
    if exist(strcat(BEM_inputs_path,'LightingFile_zone',num2str(zone),'.csv'), 'file')==2, 
        delete(strcat(BEM_inputs_path,'LightingFile_zone',num2str(zone),'.csv')); 
    end
    if exist(strcat(BEM_inputs_path,'AppliancesFile_zone',num2str(zone),'.csv'), 'file')==2, 
        delete(strcat(BEM_inputs_path,'AppliancesFile_zone',num2str(zone),'.csv')); 
    end
end

%% LOAD AND PROCESS THE DATA

%initialize some variables
occupancy_active_per_zone = [];
occupancy_sleeping_per_zone = [];
lighting_demand_per_zone = [];
appliance_demand_per_zone = [];
temperature_setpoints_per_zone = [];

for sets = 1:number_of_buildings / number_of_buildings_per_set
    
    disp(['starting set ' num2str(sets)]);
    
    occupancy_data = [occupancy_data_stem num2str(sets) '.mat'];
    load(occupancy_data);
        
    disp('occupancy data loaded')

    %iterate through the buildings, loading the data for each in sequence
    for building_number=1:number_of_buildings_per_set  

        disp(['processing building number ' num2str(building_number)]);

        %extract the data within the specified range - 3 occupant household
        occupancy3_currentbuilding = occupancy3(:,building_number);
        lighting3_currentbuilding = lighting3(:,building_number);
        appliances3_currentbuilding = appliances3(:,building_number);

        %sum the data from the households to get the aggregate numbers
        occupancy_total = occupancy3_currentbuilding(:,1);
        lighting_total = lighting3_currentbuilding(:,1);
        appliances_total = appliances3_currentbuilding(:,1);

        disp('done loading data for that building');

        %find the number of occupants in the evening and morning periods
        evening_beginning = 20 * 60; 
        evening_ending = 24 * 60;
        morning_beginning = 4 * 60;
        morning_ending = 11 * 60;

        occupancy_active = occupancy_total;
        occupancy_sleeping = [];
        occupancy_total = [];

        %iterate through time intervals
        for j=1:1440:length(occupancy_active(:,1))

            occupancy_active_current = occupancy_active(j:j + 1439,1);

            max_morning_occupancy = max(occupancy_active_current(morning_beginning:morning_ending));
            max_evening_occupancy = max(occupancy_active_current(evening_beginning:evening_ending));

            %overnight occupancy is the smaller of the two values
            overnight_occupancy = min(max_morning_occupancy,max_evening_occupancy);

            %assign this value to all timesteps between the last peak in evening
            %  occupancy and the first peak in morning occupancy
            last_occupancy_peak = max(find(occupancy_active_current == max_evening_occupancy));
            first_occupancy_peak = min(find(occupancy_active_current(morning_beginning:morning_ending) == max_morning_occupancy)) + morning_beginning;
            occupancy_total_current = occupancy_active_current;
            occupancy_total_current(last_occupancy_peak + 1:1440,1) = overnight_occupancy;
            occupancy_total_current(1:first_occupancy_peak - 1,1) = overnight_occupancy;
            occupancy_sleeping_current = occupancy_total_current - occupancy_active_current;

            %adjust for times when this procedure fails
            failed_timesteps = find(occupancy_active_current > occupancy_total_current);
            occupancy_total_current(failed_timesteps,:) = occupancy_active_current(failed_timesteps,:);

            failed_timesteps = find(occupancy_sleeping_current < 0);
            occupancy_sleeping_current(failed_timesteps,:) = 0;

            %concatenate the matrices
            occupancy_sleeping = vertcat(occupancy_sleeping, occupancy_sleeping_current);
            occupancy_total = vertcat(occupancy_total, occupancy_total_current);

        end


        % CALCULATE THE TEMPERATURE SET POINTS, LIGHTING DEMAND AND APPLICANCE DEMAND

        %initialize a matrix for the temperature setpoints
        temperature_setpoints = zeros(size(occupancy_active));

        %create the matrices
        for i=1:length(occupancy_active(1,:))
            temperature_setpoints(:,i) = zeros(length(occupancy_sleeping(:,1)),1);
            temperature_setpoints(find(occupancy_sleeping(:,i) > 0),i) = round(normrnd(18,1) * 10) / 10;
            temperature_setpoints(find(occupancy_sleeping(:,i) <= 0),i) = round(normrnd(21,1) * 10) / 10;
            temperature_setpoints(find(occupancy_total(:,i) == 0),i) = round(normrnd(12,1) * 10) / 10;
        end
        lighting_demand = lighting_total;
        appliance_demand = appliances_total;

        % divide occupancy, lighting and appliance values into values for each
        % zone.  assume equal values for each zone.
        occupancy_active_per_zone = horzcat(occupancy_active_per_zone, occupancy_active / number_of_zones);
        occupancy_sleeping_per_zone = horzcat(occupancy_sleeping_per_zone, occupancy_sleeping / number_of_zones);
        lighting_demand_per_zone = horzcat(lighting_demand_per_zone, lighting_demand / number_of_zones);
        appliance_demand_per_zone = horzcat(appliance_demand_per_zone, appliance_demand / number_of_zones);
        temperature_setpoints_per_zone = horzcat(temperature_setpoints_per_zone, temperature_setpoints);

        disp('done processing data for that building');
        disp(' ');

    end
end

disp('writing the csv files');

% CREATE SCHEDULE FILES FOR ENERGY PLUS
%write the schedules to CSV files
for zone=1:number_of_zones
    disp(['writing the csv files for zone ' num2str(zone)]);
    dlmwrite(strcat(BEM_inputs_path,'ActiveOccFile_zone',num2str(zone),'.csv'),occupancy_active_per_zone,'delimiter',',','roffset',0);
    dlmwrite(strcat(BEM_inputs_path,'SleepingOccFile_zone',num2str(zone),'.csv'),occupancy_sleeping_per_zone,'delimiter',',','roffset',0);
    dlmwrite(strcat(BEM_inputs_path,'SetpointsFile_zone',num2str(zone),'.csv'),temperature_setpoints_per_zone,'delimiter',',','roffset',0);
    dlmwrite(strcat(BEM_inputs_path,'LightingFile_zone',num2str(zone),'.csv'),lighting_demand_per_zone,'delimiter',',','roffset',0);
    dlmwrite(strcat(BEM_inputs_path,'AppliancesFile_zone',num2str(zone),'.csv'),appliance_demand_per_zone,'delimiter',',','roffset',0);
end

disp(['finished set ' num2str(sets)]);

%% WRITE THE IDF FILE - Rheinfelden

new_IDF = [new_IDF_path new_IDF_name];

%create a new IDF file
copyfile(template_IDF,new_IDF);
fileID = fopen(new_IDF,'a');

%set the relevant string values
strs = {'id_occ','id_set','id_lig','id_app',...
    'ActiveOccFile_zone1','SleepingOccFile_zone1','SetpointsFile_zone1','LightingFile_zone1','AppliancesFile_zone1'};
columns = '';

%write the strings to the file
for i=1:number_of_buildings
    columns = strcat(columns,',',num2str(i));
end
for s=1:length(strs)
    if s < 5
        strs{s} = ['Parametric:SetValueForRun,$' strs{s} columns ';'];
    else
        strs{s} = ['Parametric:SetValueForRun,$' strs{s} ',' BEM_inputs_path strs{s} '.csv' ';'];
    end
	fprintf(fileID,'%s\n',strs{s});
end

fclose(fileID);

