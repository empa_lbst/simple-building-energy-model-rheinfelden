% insert info here

%% SET THE PARAMS

%WARNING: THE MATLAB PATH MUST BE SET TO C:\EnergyPlusV8-2-7

%TODO: I think eplus is outputting data for 2 years for some reason.

clear;clc;

IDF_path = 'Rheinfelden'; %do not include the .idf extension in the filename
EPW_path = 'Rheinfelden'; %do not include the .epw extension in the filename
Eplus_path = 'RunParam_Edited';

%% RUN ENERGY PLUS

tic % start stopwatch timer to measure performance

[out.status,out.output] = system([Eplus_path ' ' IDF_path ' ' EPW_path]);

timetaken = toc;
fprintf(' Done (%g seconds).\n',timetaken); % report

