# Explanation of the module's contents #

This module includes four files:

1. **build_eplus_v2.m**: Constructs an EnergyPlus file for parametric runs using Rheinfelden_Template_Simplified.idf as a basis

1. **run_eplus.m**: Executes a batch run of the constructed EnergyPlus model.

1. **Rheinfelden_Template_Simplified.idf**: A template EnergyPlus model of a hypothetical 3 occupant, 100m2 structure in the Rheinfelden municipality of Switzerland, represented as a simple single zone structure connected with a heat network.  build_eplus_v2.m populates this file with occupancy and thermal setpoint data in the form of parametric objects.

1. **Rheinfelden.epw**: A weather file for the Rheinfelden municipality of Switzerland, which serves as input to the above IDF file. 

The files above require Matlab and EnergyPlus to run.